const jwt = require('jsonwebtoken')

const ClientError = require('./ClientError')
const logger = require('./logger')

const getCredentials = req => {
  if (req.headers && req.headers.authorization) {
    const parts = req.headers.authorization.split(' ')
    if (parts.length === 2) {
      const [scheme, credentials] = parts
      if (/^Bearer$/i.test(scheme)) {
        return credentials
      }
    }
    throw new ClientError('CREDENTIALS_BAD_SCHEME', 400)
  } else {
    throw new ClientError('AUTHORIZATION_REQUIRED', 401)
  }
}

const getToken = (credentials, options, publicKeys = {}) => {
  if (!credentials) {
    throw new ClientError('CREDENTIALS_REQUIRED', 401)
  }

  const { header } = jwt.decode(credentials, { complete: true }) || {}

  if (!header) {
    throw new ClientError('INVALID_TOKEN', 400)
  }
  if (!header.kid) {
    throw new ClientError('KID_NOT_FOUND', 400)
  }

  const publicKey = publicKeys[header.kid]

  if (!publicKey) {
    throw new ClientError('PUBLIC_KEY_NOT_FOUND', 400)
  }

  try {
    return jwt.verify(credentials, publicKey, options)
  } catch (err) {
    logger.error(err)
    throw new ClientError('AUTHORIZATION_REQUIRED', 401, err)
  }
}

const createToken = ({ privateKey, audience, issuer }) => {
  return jwt.sign({}, privateKey, {
    audience,
    issuer,
    algorithm: 'RS256',
    keyid: issuer
  })
}

module.exports = {
  getCredentials,
  getToken,
  createToken
}
