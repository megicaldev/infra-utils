const split = require('split')
const jsonStringify = require('fast-safe-stringify')
const formatDate = require('date-fns/format')
const parseISO = require('date-fns/parseISO')

const winston = require('winston')
const { transports, format } = winston
const { combine, timestamp, printf } = format

const LOGGER_LEVEL = process.env.LOGGER_LEVEL || 'info'

const formatter = printf(({ timestamp, level, message, stack }) => {
  const formattedTimesstamp = formatDate(
    parseISO(timestamp),
    'yyyy-MM-dd HH:mm:ss.SSS'
  )
  const output = `[${formattedTimesstamp}] [${level.toUpperCase()}] ${message}`

  if (stack) {
    return output + `\n[${formattedTimesstamp}] [STACK] ${stack}`
  } else {
    return output
  }
})

const jsonFormatter = printf(({ timestamp, level, message, stack }) => {
  return jsonStringify({ timestamp, level, message, stack })
})

const parseFormat = (format) => {
  const loggerFormat = format || 'json'
  if (loggerFormat === 'combine') {
    return combine(timestamp(), formatter)
  } else {
    return combine(timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }), jsonFormatter)
  }
}

const logStream = (logger) =>
  split().on('data', (message) => {
    logger.info(message)
  })

const getRandomInt = (max) => Math.floor(Math.random() * Math.floor(max))

const loggerWithFormat = (format, level = LOGGER_LEVEL) => {
  const name = `withFormat_${getRandomInt}`
  winston.loggers.add(name, {
    level,
    format: parseFormat(format),
    transports: [new transports.Console()]
  })
  return {
    logger: winston.loggers.get(name),
    logStream: logStream(winston.loggers.get(name))
  }
}

winston.loggers.add('default', {
  level: LOGGER_LEVEL,
  format: parseFormat(process.env.LOGGER_FORMAT),
  transports: [new transports.Console()]
})

module.exports = winston.loggers.get('default')
module.exports.logStream = logStream(winston.loggers.get('default'))
module.exports.loggerWithFormat = loggerWithFormat
