class ClientError extends Error {
  constructor(code, status, info, logMessage) {
    super(code)
    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ClientError)
    }

    this.code = code || 'USER_ERROR'
    this.status = status || 400
    this.info = info || ''
    this.logMessage = logMessage || ''
  }
}

module.exports = ClientError
