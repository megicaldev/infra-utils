const migrations = require('sql-migrations')

var args = process.argv.slice(2)
if (args[0] !== 'create') {
  console.log('Create parameter missing')
  process.exit()
}
if (!args[1]) {
  console.log('Migration name missing')
  process.exit()
}

migrations.run({ migrationsDir: 'migrations' })
