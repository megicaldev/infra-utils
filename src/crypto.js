const crypto = require('crypto')
const util = require('util')
const x509 = require('@ghaiklor/x509')
const base64url = require('base64url')
const EC = require('elliptic').ec
const ClientError = require('./ClientError')

const randomBytes = util.promisify(crypto.randomBytes)
const verifyChain = util.promisify(x509.verifyChain)

const challenge = async () => {
  try {
    return randomBytes(32)
  } catch (error) {
    throw new ClientError('AUTH_ERROR', 400, 'Error generating challenge')
  }
}

const challengeAsEcPoint = async () => {
  try {
    const ecdh = crypto.createECDH('secp384r1')
    return { ecdh: ecdh, ecdhPubKey: ecdh.generateKeys().toString('hex') }
  } catch (error) {
    throw new ClientError('AUTH_ERROR', 400, 'Error generating challenge')
  }
}

const bufferToBase64 = (buffer) => {
  if (!Buffer.isBuffer(buffer)) {
    throw Error('Not a buffer')
  }
  return buffer
    .toString('base64')
    .match(/.{0,64}/g)
    .join('\n')
    .trim()
}

const certPemFromBuffer = (buffer) => `-----BEGIN CERTIFICATE-----
${bufferToBase64(buffer)}
-----END CERTIFICATE-----`

const crlPemFromBuffer = (buffer) => `-----BEGIN X509 CRL-----
${bufferToBase64(buffer)}
-----END X509 CRL-----`

const publicKeyFromBuffer = (buffer) => `-----BEGIN PUBLIC KEY-----
${bufferToBase64(buffer)}
-----END PUBLIC KEY-----`

const parseCert = (pem) => x509.parseCert(pem)

const parsePlainPem = (pem) => {
  let plainCertData = pem.replace('-----BEGIN PUBLIC KEY-----', '')
  plainCertData = plainCertData.replace('-----END PUBLIC KEY-----', '')
  return plainCertData.replace(/(\r\n|\n|\r)/gm, '')
}

const validateKeyType = (type, key) => {
  if (key.asymmetricKeyType !== type) {
    throw Error(
      `Invalid public key type ('${type}' != '${key.asymmetricKeyType}')`
    )
  }
}

const parseEcPubkeyToRaw = (ec, publickey) => {
  const DER_HDR_SIZE = 23
  const pubKeyDer = publickey.export({ type: 'spki', format: 'der' }).slice(DER_HDR_SIZE)
  return ec.keyFromPublic(pubKeyDer, 'hex')
}

module.exports = (logger, debug = false) => {
  // returns true or throws error
  const validateCertChain = async (
    cert,
    caCert,
    crl,
    intermediateCert,
    intermediateCrl
  ) => {
    try {
      await verifyChain(cert, caCert, crl, intermediateCert, intermediateCrl)
      return true
    } catch (err) {
      logger.debug(err)
      throw new ClientError(
        'AUTH_ERROR',
        400,
        'Error validating certificate chain'
      )
    }
  }

  const defaultSignatureType = 'sha1WithRSAEncryption'
  const defaultHashType = 'SHA256'

  const rsa = {
    digest: (data, hashType = defaultHashType) =>
      crypto.createHash(hashType).update(data).digest(),

    sign: ({ privateKey, data, signatureType = defaultSignatureType }) => {
      validateKeyType('rsa', privateKey)
      const sign = crypto.createSign(signatureType)
      sign.write(data)
      sign.end()
      const signature = sign.sign(privateKey)

      debug &&
        logger.debug(`RSA Sign:
${privateKey.type}:${privateKey.asymmetricKeyType}
Data: ${data.toString('hex')}
Signature: ${signature.toString('hex')} (${signature.length})
    `)

      return base64url.encode(signature)
    },

    verify: ({
      publicKey,
      data,
      signature,
      signatureType = defaultSignatureType
    }) => {
      try {
        validateKeyType('rsa', publicKey)
        const verify = crypto.createVerify(signatureType)
        verify.write(data)
        verify.end()

        const sig = Buffer.from(signature, 'base64')

        debug &&
          logger.debug(`RSA Verify:
${publicKey.type}:${publicKey.asymmetricKeyType}
Data: ${data.toString('hex')}
Signature: ${sig.toString('hex')} (${sig.length})
    `)

        return verify.verify(
          {
            key: publicKey
          },
          sig
        )
      } catch (error) {
        logger.debug(error)
        return false
      }
    },

    encrypt: ({ publicKey, data }) => {
      validateKeyType('rsa', publicKey)
      const encrypted = base64url.encode(
        crypto.publicEncrypt(
          { key: publicKey, padding: crypto.constants.RSA_PKCS1_PADDING },
          data
        )
      )

      debug &&
        logger.debug(`RSA Encrypt:
${publicKey.type}:${publicKey.asymmetricKeyType}
Data: ${base64url.encode(data)}
encrypted: ${encrypted}
        `)

      return encrypted
    },

    decrypt: ({ privateKey, data }) => {
      validateKeyType('rsa', privateKey)
      return crypto.privateDecrypt(
        { key: privateKey, padding: crypto.constants.RSA_PKCS1_PADDING },
        Buffer.from(data, 'base64')
      )
    }
  }

  const defaultEcSignatureType = 'sha256'

  const ec = {
    digest: (data, hashType = defaultHashType) =>
      crypto.createHash(hashType).update(data).digest(),

    sign: ({ privateKey, data, signatureType = defaultEcSignatureType }) => {
      validateKeyType('ec', privateKey)
      const sign = crypto.createSign(signatureType)
      sign.write(data)
      sign.end()
      const signature = sign.sign(privateKey)

      debug &&
        logger.debug(`EC Sign:
${privateKey.type}:${privateKey.asymmetricKeyType}
Data: ${data.toString('hex')}
Signature: ${signature.toString('hex')} (${signature.length})
    `)

      return base64url.encode(signature)
    },

    verify: ({
      publicKey,
      data,
      signature,
      signatureType = defaultEcSignatureType
    }) => {
      try {
        validateKeyType('ec', publicKey)
        const verify = crypto.createVerify(signatureType)
        verify.write(data)
        verify.end()

        const sig = Buffer.from(signature, 'base64')

        debug &&
          logger.debug(`EC Verify:
${publicKey.type}:${publicKey.asymmetricKeyType}
Data: ${data.toString('hex')}
Signature: ${sig.toString('hex')} (${sig.length})
    `)

        return verify.verify(
          {
            key: publicKey
          },
          sig
        )
      } catch (error) {
        logger.debug(error)
        return false
      }
    },

    verifyDigest: ({
      publicKey,
      digest,
      signature,
      curveType = 'p384'
    }) => {
      try {
        validateKeyType('ec', publicKey)
        const digestHex = Buffer.from(digest, 'base64').toString('hex')
        const ecSec = new EC(curveType)
        const key = parseEcPubkeyToRaw(ecSec, publicKey)

        const sig = Buffer.from(signature, 'base64')

        debug &&
          logger.debug(`EC Verify:
${publicKey.type}:${publicKey.asymmetricKeyType}
Data: ${digest.toString('hex')}
Signature: ${sig.toString('hex')} (${sig.length})
    `)
        return key.verify(digestHex, sig)
      } catch (error) {
        logger.debug(error)
        return false
      }
    },

    verifyEcdh: ({
      publicKey,
      ecdhSk,
      ecSharedSecret
    }) => {
      try {
        const EC384_POINT_SIZE = 96
        const pubKeyPem = publicKey.export({ type: 'spki', format: 'pem' })
        const pubKeyPlain = parsePlainPem(pubKeyPem)

        const pubKeyBuf = Buffer.from(pubKeyPlain, 'base64')
        const otherPubKey = pubKeyBuf.slice(pubKeyBuf.length - EC384_POINT_SIZE - 1)

        const sharedSecretBuf = Buffer.from(ecSharedSecret, 'base64')

        const ecdh = crypto.createECDH('secp384r1')
        ecdh.setPrivateKey(Buffer.from(ecdhSk, 'hex'))

        const ownSecretBuf = ecdh.computeSecret(otherPubKey)

        const otherSharedSecretBuf = Buffer.from(ecSharedSecret, 'base64')

        debug &&
          logger.debug(`ECDH Verify:
${publicKey.type}:${publicKey.asymmetricKeyType}
SharedSecret: ${sharedSecretBuf.toString('hex')}
    `)

        return otherSharedSecretBuf.toString('hex') === ownSecretBuf.toString('hex')
      } catch (error) {
        logger.debug(error)
        return false
      }
    }
  }

  const virtual = {
    verify: ({
      publicKey,
      data,
      signature,
      signatureType = 'SHA256',
      vpk,
      vpkBuf,
      vpkSignature
    }) => {
      // Verify signature over challenge
      try {
        console.log('Start virtual verify')
        validateKeyType('ec', vpk)
        const verify = crypto.createVerify(signatureType)
        verify.write(data)
        verify.end()

        const sig = Buffer.from(signature, 'base64')

        debug &&
          logger.debug(`EC Verify:
${publicKey.type}:${vpk.asymmetricKeyType}
Data: ${data.toString('hex')}
Signature: ${sig.toString('hex')} (${sig.length})
    `)
        if (verify.verify({ key: vpk }, sig)) {
          try {
            if (!(publicKey.asymmetricKeyType === 'rsa' || publicKey.asymmetricKeyType === 'ec')) {
              throw Error(
                `Invalid public key type ('${publicKey.asymmetricKeyType}')`
              )
            }
            let verifyResult
            if (publicKey.asymmetricKeyType === 'ec') {
              const dataDigest = crypto.createHash('sha256').update(vpkBuf).digest('base64')
              verifyResult = ec.verifyDigest({
                publicKey,
                digest: dataDigest,
                signature: vpkSignature
              })
            } else {
              const verify = crypto.createVerify('sha384')
              verify.write(vpkBuf)
              verify.end()

              const sig = Buffer.from(vpkSignature, 'base64')

              debug &&
                logger.debug(`Signature Verify:
      ${publicKey.type}:${publicKey.asymmetricKeyType}
      Data: ${vpkBuf.toString('hex')}
      Signature: ${sig.toString('hex')} (${sig.length})
          `)
              verifyResult = verify.verify({ key: publicKey }, sig)

              if (verifyResult === false) {
                const verify = crypto.createVerify('sha256')
                verify.write(vpkBuf)
                verify.end()

                const sig = Buffer.from(vpkSignature, 'base64')

                debug &&
                  logger.debug(`Signature Verify:
        ${publicKey.type}:${publicKey.asymmetricKeyType}
        Data: ${vpkBuf.toString('hex')}
        Signature: ${sig.toString('hex')} (${sig.length})
            `)
                verifyResult = verify.verify({ key: publicKey }, sig)
              }
            }
            if (verifyResult === true) {
              console.log('Wallet signature verified')
            } else {
              console.log('Wallet signature verify failed')
            }
            return verifyResult
          } catch (error) {
            logger.debug(error)
            return false
          }
        } else {
          logger.debug('Virtual card signature verify failed')
          return false
        }
      } catch (error) {
        logger.debug(error)
        return false
      }
    }
  }

  const algorithm = 'aes-256-gcm'
  const authTagLengthInBytes = 16
  const keyLengthInBytes = 32
  const ivLengthInBytes = 12
  const aes = {
    encrypt: (
      data,
      key = Buffer.from(crypto.randomBytes(keyLengthInBytes)),
      iv = Buffer.from(crypto.randomBytes(ivLengthInBytes))
    ) => {
      const cipher = crypto.createCipheriv(algorithm, key, iv, {
        authTagLength: authTagLengthInBytes
      })

      // for better performance stream api could be used
      const enc = cipher.update(data, 'utf8')
      const final = cipher.final()

      const authTag = cipher.getAuthTag()

      const encrypted = Buffer.concat([iv, enc, final, authTag])

      debug &&
        logger.debug(`Aes encrypt:
Key: ${key.toString('hex')}
IV: ${iv.toString('hex')}
authTag: ${authTag.toString('hex')}
Data: ${data}
Encrypted: ${encrypted.toString('hex')}
        `)

      return { key, encrypted: base64url.encode(encrypted) }
    },

    decrypt: ({ encrypted, key }) => {
      const data = Buffer.from(encrypted, 'base64')

      const iv = data.subarray(0, ivLengthInBytes)
      const authTag = data.subarray(data.length - authTagLengthInBytes)

      const content = data.subarray(
        ivLengthInBytes,
        data.length - authTagLengthInBytes
      )

      const decipher = crypto.createDecipheriv(algorithm, key, iv, {
        authTagLength: authTagLengthInBytes
      })
      decipher.setAuthTag(authTag)

      debug &&
        logger.debug(`Aes decrypt:
Key: ${key.toString('hex')} (${key.length})
IV: ${iv.toString('hex')} (${iv.length})
authTag: ${authTag.toString('hex')} (${authTag.length})
content: ${content.toString('hex')} (${content.length})
data: ${data.toString('hex')} (${data.length})
        `)

      const decrypted = Buffer.concat([
        decipher.update(content),
        decipher.final()
      ])

      debug && logger.debug(`Decrypted: ${decrypted}`)

      return decrypted
    }
  }

  return {
    challenge,
    challengeAsEcPoint,
    parseCert,
    parsePlainPem,
    validateCertChain,
    crlPemFromBuffer,
    certPemFromBuffer,
    publicKeyFromBuffer,
    rsa,
    ec,
    virtual,
    aes,
    base64url,
    crypto
  }
}
