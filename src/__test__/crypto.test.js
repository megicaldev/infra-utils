const { generateKeyPairSync } = require('crypto')
const nodeCrypto = require('crypto')
const crypto = require('../crypto')(console, true)
const fs = require('fs')
const path = require('path')
const { base64url } = crypto

describe('crypto', () => {
  test('challenge', async () => {
    expect(await crypto.challenge()).toBeDefined()
  })

  test('digest', async () => {
    expect(await crypto.rsa.digest('test').toString('hex')).toBe(
      '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08'
    )
  })

  test('x509', async () => {
    const certPem = fs
      .readFileSync(path.resolve(__dirname, 'cryptoKeys', 'user_cert.pem'))
      .toString()
    const userCert = await crypto.parseCert(certPem)

    expect(userCert.subject.commonName).toBe('John Doe')
    expect(userCert.subject.serialNumber).toBe('12345678910')

    // Test certificate chain validation with old CRL
    const cert = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'vrk_user_cert.crt')
    )
    const intermediateCA = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'vrk_test_intermediate.crt')
    )
    const rootCA = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'vrk_test_root.crt')
    )
    const crl = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'crl_old.pem')
    )
    const intermediateCrl = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'crl_ca.pem')
    )
    const userCertCrl = fs.readFileSync(
      path.resolve(__dirname, 'testCerts', 'hightrust_crl.pem')
    )
    const userCertCA = fs.readFileSync(
      path.resolve(__dirname, 'testCerts', 'hightrust_ca.pem')
    )
    const userCertRevoked = fs.readFileSync(
      path.resolve(__dirname, 'testCerts', 'hightrust_user.pem')
    )

    await expect(
      crypto.validateCertChain(
        userCertRevoked,
        userCertCA,
        userCertCrl,
        userCertCA,
        userCertCrl
      )
    ).rejects.toThrow(Error('AUTH_ERROR'))

    await expect(
      crypto.validateCertChain(
        crypto.certPemFromBuffer(cert),
        crypto.certPemFromBuffer(rootCA),
        crl,
        crypto.certPemFromBuffer(intermediateCA),
        intermediateCrl
      )
    ).rejects.toThrow(Error('AUTH_ERROR'))
  })

  test('dvv_x509_without_crls', async () => {
    const certPem = fs
      .readFileSync(path.resolve(__dirname, 'cryptoKeys', 'cert_from_card.pem'))
      .toString()
    const userCert = await crypto.parseCert(certPem)

    expect(userCert.subject.commonName).toBe('Kauranen Sigvard 00198704892')
    expect(userCert.subject.serialNumber).toBe('00198704892')

    // Test certificate chain validation with old CRL
    const cert = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'cert_from_card.pem')
    )
    const intermediateCA = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'CA_DVV_TEST')
    )
    const rootCA = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'CA_ROOT_DVV_TEST')
    )

    let res
    try {
      await crypto.validateCertChain(
        cert,
        crypto.certPemFromBuffer(rootCA),
        '',
        crypto.certPemFromBuffer(intermediateCA),
        ''
      )
      res = true
    } catch (err) {
      res = false
    }

    expect(res).toBe(true)
  })

  test('dvv_x509_with_old_crls_should_fail', async () => {
    const certPem = fs
      .readFileSync(path.resolve(__dirname, 'cryptoKeys', 'cert_from_card.pem'))
      .toString()
    const userCert = await crypto.parseCert(certPem)

    expect(userCert.subject.commonName).toBe('Kauranen Sigvard 00198704892')
    expect(userCert.subject.serialNumber).toBe('00198704892')

    // Test certificate chain validation with old CRL
    const cert = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'cert_from_card.pem')
    )
    const intermediateCA = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'CA_DVV_TEST')
    )
    const rootCA = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'CA_ROOT_DVV_TEST')
    )
    const crl = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'vrktshcpc.crl')
    )
    const intermediateCrl = fs.readFileSync(
      path.resolve(__dirname, 'cryptoKeys', 'vrktest2a.crl')
    )

    let res
    try {
      await crypto.validateCertChain(
        cert,
        crypto.certPemFromBuffer(rootCA),
        crl,
        crypto.certPemFromBuffer(intermediateCA),
        intermediateCrl
      )
      res = true
    } catch (err) {
      res = false
    }

    expect(res).toBe(false)
  })

  describe('rsa', () => {
    const { publicKey, privateKey } = generateKeyPairSync('rsa', {
      modulusLength: 2048 + 1024
    })
    const { rsa } = crypto

    test('sign and verify', () => {
      const data = 'test'
      const signature = rsa.sign({
        privateKey,
        data,
        signatureType: 'sha384WithRSAEncryption'
      })
      expect(signature).toBeDefined()
      expect(
        rsa.verify({
          publicKey,
          data,
          signature,
          signatureType: 'SHA384'
        })
      ).toBe(true)
    })

    test('sign with private encrypt', () => {
      const data = '<ds:SignedInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:CanonicalizationMethod><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod><ds:Reference Id="r-id-a4873c5eb16f2f8971bfefe1c2e3a7a0-1" URI="detached-file"><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue>jjoXfWS7ituXfUzLfAERLgK/jEP3lg4ANcyxvjpZ3IA=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-a4873c5eb16f2f8971bfefe1c2e3a7a0"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue>ebDCmC3Q+lszWHB9UejdV/ZaDgzUje5qCd00xAPWpSQ=</ds:DigestValue></ds:Reference></ds:SignedInfo>'
      const sign = nodeCrypto.createSign('SHA256')
      sign.write(data)
      sign.end()
      const signature = sign.sign(privateKey, 'hex')
      console.log('SIGNATURE 1:' + signature)

      // 625be2467fcc44bceb182bf3d854576a288add886822353a1ed80eb47e5688a6
      const digest = nodeCrypto.createHash('SHA256').update(data).digest()
      console.log('DIGEST_2: ', digest.toString('hex'))
      const digestInfo = Buffer.from('3031300d060960864801650304020105000420', 'hex') // OID of SHA-256

      const signature2 = nodeCrypto.privateEncrypt(privateKey, Buffer.concat([digestInfo, digest]))
      console.log('SIGNATURE 2:' + signature2.toString('hex'))
    })

    test('verify data from dvv card', async () => {
      const userCert = fs.readFileSync(
        path.resolve(__dirname, 'cryptoKeys', 'testivrk-anita.pem')
      )
      const publicKey = nodeCrypto.createPublicKey(userCert)

      expect(publicKey.type).toBe('public')
      expect(publicKey.asymmetricKeyType).toBe('rsa')

      const data = Buffer.from(
        '94fcf496d2ec0318549301713619c809af79593f6fe0dcbeb7766d878f318d9c',
        'hex'
      )
      const signature =
        Buffer.from(
          '234c1f6a377e1772a906df2ba82ed3b5f96fc2ed03ad701a448045a1ec61dcc1a7b434ed80ed5de208d447da6378aa235e57bfc50b6a9d9920487b1744baaaaf54393441d6d0884af7bda575e7c48851245d478e0eda2792755b4868695b77178c0481f8a215ab09a646614b7e0e89b6d9c3091963a006483fb83dc7fb12914b77ea971841cce8cf58a1ca9dcbc8ec0f5acd09f3866d7090fb99a70675cc8d6a8735b36d2dfbe1e907747d0d33741f061ebf0f76e7c9c4c6585cbad15d5c7c91f3cf76db936d3c62d07e8514f01063a9b07b500a2064934c82d17ecd148619f35fb35ff10e4b81ad3af75c7bfb89d70f2e5fafcb8103434d31cb4c3b585eda779f7f9c8cd50f1260d576f255eea58c4e7e1f42140c4a6cb240712bd30c4fdbeff058a53623b944920dce9436204d09534458cdedeb272d296fc7efb1f6b06e2339b4480404c419e9e23374a5bab3c3d87e1e3cfd3f724a65917d95a6bba0c3008e851686a2bb490223dd15881ba4f68d8a64f634b3b83810befc71b798493002',
          'hex'
        ).toString('base64')

      const signatureType = 'SHA384'

      expect(rsa.verify({ publicKey, data, signature, signatureType })).toBe(
        true
      )
    })

    test('verify signature data from dvv card', async () => {
      const userCert = fs.readFileSync(
        path.resolve(__dirname, 'cryptoKeys', 'naukkarinen_inna_sign_cert.pem')
      )
      const publicKey = nodeCrypto.createPublicKey(userCert)

      expect(publicKey.type).toBe('public')
      expect(publicKey.asymmetricKeyType).toBe('rsa')

      const data = 'xeK2GchWzm+Xp4n5veTVZbbv768LBikhv37BVmqaitg='

      const signature = 'bL8dCZf7UWjtI2r7cNDDgQVUpoeTLm000EOue8vGYdy98ER2JChlb/HZLFe4MDebUxYZSgPeM4KbyOFn6OccoGlrQ6MBt0HmJMKJjbkDZSNDr2PlyuNyS73LnwR3XNXHQYr27r4FeVeA+gD3AnQjh0jvMPuhJKH8v0S4ofdOLJjPhkO0DOKD9BzDvM7KuGiRUlwPP1FaYtbiC+WCs3BWfhJbMB1jyTUEDq7x2Ah0thWrTTkmd9DI41RpSExJcr+P6YPRa66WJpa/xn3+G8Zifz6KgGT5vah+V+rpaSZuYYlKQVhYqU5wkiovn5NeF+A54bHj8h6X3cGWoMtCFaf7Bp9rAWEtjwLYhRNfZg/k2wwzhEEhu3L53dVbpqcwOLWLmGsQ/wC7DKk/LAy1XSX39Esh8MRvN9g07SDLPYEh78fwojZNAt+ag+3b7uJ9+aWHPwQfntFIkwIjTOsbzRQonlO1Hyohe+gQvQEQvq/6IlS1WTEf9PcgYCLQnaSrsVhr'

      const signatureType = 'sha256'

      expect(rsa.verify({ publicKey, data, signature, signatureType })).toBe(
        true
      )
    })

    test('encrypt and decrypt', () => {
      const data = 'plain text'
      const encrypted = rsa.encrypt({ publicKey, data: Buffer.from(data) })
      expect(encrypted).toBeDefined()
      expect(
        rsa.decrypt({ privateKey, data: encrypted }).toString('utf8')
      ).toBe(data)
    })

    test('wrong key type', () => {
      const { publicKey, privateKey } = generateKeyPairSync('ec', {
        namedCurve: 'P-256'
      })
      const data = 'test'
      const signature = ''

      expect(() =>
        rsa.sign({
          privateKey,
          data
        })
      ).toThrow("Invalid public key type ('rsa' != 'ec')")

      expect(
        rsa.verify({
          publicKey,
          data,
          signature,
          signatureType: 'SHA384'
        })
      ).toBe(false)
    })
  })

  describe('ec', () => {
    const { ec } = crypto

    test('sign and verify', () => {
      const { publicKey, privateKey } = generateKeyPairSync('ec', {
        namedCurve: 'P-256'
      })
      const data = 'EC signature test'
      const signature = ec.sign({
        privateKey,
        data,
        signatureType: 'sha256'
      })
      expect(signature).toBeDefined()
      expect(
        ec.verify({
          publicKey,
          data,
          signature,
          signatureType: 'SHA256'
        })
      ).toBe(true)
    })

    test('verify ecdsa', async () => {
      const data = 'BKyJu2pcXghiShjhKOIdbn/3ta4MK/erkOTZNaM2vLU='
      const digest = nodeCrypto
        .createHash('sha256')
        .update(data)
        .digest('base64')

      const certPem = fs
        .readFileSync(path.resolve(__dirname, 'cryptoKeys', 'Koivukangas_Katri_00198704199.pem'))
        .toString()
      const publicKey = nodeCrypto.createPublicKey(certPem)
      const signature = 'MGQCMKf8wlq8uKhX9-aavPrbxbzZ4GkJ4i7Lu6CEXqNFyVkMxCIUIz9HQXEiooKNkQymNgIwdckmBpSpKxWPQWiaSW4TN7Pp5dmrHNvqxTIEbLnqtvJBDMOeGSBrKXa7pfbKx9Dv'

      const res = crypto.ec.verifyDigest({ publicKey, digest, signature })

      expect(res).toBe(true)
    })

    test('verify ecdh-384 secret from dvv ecc card', async () => {
      const KEY_HDR = Buffer.from('3076301006072a8648ce3d020106052b81040022036200', 'hex')
      const ecdhCard = nodeCrypto.createECDH('secp384r1')
      const cardPubKeyRawBuf = ecdhCard.generateKeys()
      const cardPubKeyBuf = Buffer.concat([KEY_HDR, cardPubKeyRawBuf])
      const cardPubKeyPem = crypto.publicKeyFromBuffer(cardPubKeyBuf)
      const cardPK = nodeCrypto.createPublicKey(cardPubKeyPem)

      const ecdh = nodeCrypto.createECDH('secp384r1')
      const myKey = ecdh.generateKeys()
      const ecdhSk = ecdh.getPrivateKey().toString('hex')

      const otherSharedSecret = ecdhCard.computeSecret(myKey).toString('base64')

      const result = crypto.ec.verifyEcdh({ publicKey: cardPK, ecdhSk, ecSharedSecret: otherSharedSecret })
      expect(result).toBe(true)
    })

    test('verify ecdh-384 secret with wrong shared secret', async () => {
      const KEY_HDR = Buffer.from('3076301006072a8648ce3d020106052b81040022036200', 'hex')
      const ecdhCard = nodeCrypto.createECDH('secp384r1')
      const cardPubKeyRawBuf = ecdhCard.generateKeys()
      const cardPubKeyBuf = Buffer.concat([KEY_HDR, cardPubKeyRawBuf])
      const cardPubKeyPem = crypto.publicKeyFromBuffer(cardPubKeyBuf)
      const cardPK = nodeCrypto.createPublicKey(cardPubKeyPem)

      const ecdh = nodeCrypto.createECDH('secp384r1')
      ecdh.generateKeys()
      const ecdhSk = ecdh.getPrivateKey().toString('hex')

      const otherSharedSecret = Buffer.from('de50ce0884ce59e61b4944cf84306ac76620b9c974459a499cbe558c136c32f3b36e1039d34192969a63b54763861f3d').toString('base64')

      const result = crypto.ec.verifyEcdh({ publicKey: cardPK, ecdhSk, ecSharedSecret: otherSharedSecret })
      expect(result).toBe(false)
    })

    test('wrong key type', () => {
      const { publicKey, privateKey } = generateKeyPairSync('rsa', {
        modulusLength: 1024
      })
      const data = 'EC signature test'
      expect(() =>
        ec.sign({
          privateKey,
          data
        })
      ).toThrow("Invalid public key type ('ec' != 'rsa')")

      expect(
        ec.verify({
          publicKey,
          data,
          signature: 'signature'
        })
      ).toBe(false)
    })
  })

  describe('virtual', () => {
    const { virtual } = crypto
    test('verify EC - 1', () => {
      const certPem = fs
        .readFileSync(path.resolve(__dirname, 'cryptoKeys', 'Koivukangas_Katri_00198704199_ec_auth.pem'))
        .toString()
      const publicKey = nodeCrypto.createPublicKey(certPem)

      const loginCode = '344926900'
      const challenge = 'BNxxgrUrFng8YVAJj8XWdyokp2JHlm/8pFqGTwXQi4NBnNmHLUTFv0r0FeRCSZqyj2+cyrgFrR29BVNRMrE8eAR1PSKXrjQ7+2YXRDxwAAluLbUTlKw+GTB3rujNgM4c4Q=='
      const data = Buffer.concat([
        Buffer.from(challenge, 'base64'),
        Buffer.from(loginCode, 'utf8')
      ])

      const cardSignature = 'MEUCIQCMr8UprfTWc5TkSVPacu5nyFOAiGm-RzNGc0M-BxD4lgIgHvKOl7ycwIWcBqBDSBy5vgCBcM2GvAgdmPQzF7Hmu-U'

      const vpkData = 'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEyx78_RNHgibEZBn70jdHSN6Fn7Tk0CQL_hCeShnMiVAhPDq4lC7A6S1x0Hj5fEQqOvuDLFQ0mHmJaeFR_Rp9dA'
      const vpkBuf = Buffer.from(vpkData, 'base64')
      const vpk = nodeCrypto.createPublicKey({ key: vpkBuf, format: 'der', type: 'spki' })
      console.log('VPK-ville', vpkBuf.toString('hex'))
      const vpkSignature = 'MGQCMJw-9rdSYQI5eyt6LVQRXjkb1XY2jqMfwjuLtYtlpR6glEqSeR3594vWNa7NmhyMrgIw6YQnE4ufRGuYHyQWE-v_c1VGgFI_QC53yBAxiRkfwYgwzez5j7DakXZgqGLyPrhl'

      expect(
        virtual.verify({
          publicKey,
          data,
          signature: cardSignature,
          signatureType: 'SHA256',
          vpk,
          vpkBuf,
          vpkSignature
        })
      ).toBe(true)
    })

    test('verify EC - 2', () => {
      const certPem = fs
        .readFileSync(path.resolve(__dirname, 'cryptoKeys', 'auth_cert_2_ec.pem'))
        .toString()
      const publicKey = nodeCrypto.createPublicKey(certPem)

      const dataHex = '04766aeb0272bebaba8925a0ec685fb46cc20876691a4aefc995d6b4b58266d524040883d936aeadc78c7d37c8afb72eefa940ebfd902dff1aa40d9c67f3c1be64e28b01ce702399891134d9aeb29ec7c5e2b66f46d747372502bfae190f110a13383835323832363737'
      const data = Buffer.from(dataHex, 'hex')

      const cardSignature = 'MEUCIEnG+VRAsdIPuQIFtfL+E8Uw+xgSHLtrooxzLpALUvYdAiEAgZTZAMywsz7gYPPwz6UqdouFQUWfAVEdUrN5MjFPuPU='

      const vpkData = 'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEmIrb888mdls8UIo3wTHjlsG39eyZoomyzGmL8eIacZJ1OTCXo2xdhyFc8j/swNBX+sJYzr57Mb2/QANzRNmewQ=='
      const vpkBuf = Buffer.from(vpkData, 'base64')
      const vpk = nodeCrypto.createPublicKey({ key: vpkBuf, format: 'der', type: 'spki' })
      const vpkSignature = 'MGQCML9yQv8qDp9SqSMGZxgp3e0udI1SeB9E9qP5ZojKnEueLeehYcuSkgIrkofcQigM9QIwCLD8uq9RJWyzUUPevJcHlbyqGuG3qwLf6GpFmn1t9TNqmiiLbPs0yqRr5lQlvOsO'
      expect(
        virtual.verify({
          publicKey,
          data,
          signature: cardSignature,
          signatureType: 'SHA256',
          vpk,
          vpkBuf,
          vpkSignature
        })
      ).toBe(true)
    })

    test('verify RSA', () => {
      const certPem = fs
        .readFileSync(path.resolve(__dirname, 'cryptoKeys', 'wallet_rsa_sig_cert.pem'))
        .toString()
      const publicKey = nodeCrypto.createPublicKey(certPem)
      const vpk = 'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEg8JJutzdHg79pOSweOpGhtKjlJJgUgrzmT06hRcqKljE7LFNLKn3A3hao1prrL75VK/i46v7a2CqJaDqABjf5A=='
      const vpkSignature = 'KArSZCSY8y4XwuPnkVfL1V4uQMt9Gs8bKRTTZQVhR8CwO0Sj+xKLzXcswxgwhaAjRh/Ny1xS8ZMIliLj5PsovZzarLk+00ZaUUQ8RooXiQ+g8NOtxH/kmkAtUPpVb/UfOPngsz9fSn3i+uFWiZOK3r6pnAt1UlQjG9bgVfu99ZlYp8EaQ1sl5jaKGZukB3jLQBY662zQOsPY6+BdYBxCRbu0eY64ZMh/MldvL2rZbbFVrt/VW3wzG4kLbF8XcIYBhrwNQP2e2sA6FkumHitgn6QnNBAHiIihYM+OZ5hSc0DKeYpp6HBF7mFP/GCX6IvTujcmSC0Xy/P44WfCljbwNs/TRs2NAXlJdPbabD/6hN7k0g8IuNliXRoVWjOYaNPdPmJwM+DH54MUKBxuzs/8jeQnwxV5YqEdGnPtrM2qLxa6UaifJgCvH+tfdGgA1BON9jbmYXh3OBF2EP6XIEecNsOAXXDnBzLJWuktsdX+N5IoOwHwB7FleE0wdkNLWzyr'

      const vpkBuf = Buffer.from(vpk, 'base64')
      const vpkAsKey = nodeCrypto.createPublicKey({ key: vpkBuf, format: 'der', type: 'spki' })

      const dataStr = 'Voluptas iste dolorem porro.'
      const data = nodeCrypto.createHash('sha256').update(dataStr).digest('base64')

      const signature = 'MEQCIHRXQaumoUKJEVg8mjrLzqMZCd4hSZjKg6ckAyY94rWQAiB8DMekneo7a5c0pHBQWPAKBOK8reTiAHTZwrig6xbbQQ=='

      expect(virtual.verify({
        publicKey, // keyObject
        data, // base64
        signature, // base64
        signatureType: 'SHA256',
        vpk: vpkAsKey, // keyObject
        vpkBuf, // buffer from base64 der
        vpkSignature // base64
      })).toBe(true)
    })
  })

  describe('aes', () => {
    test('encrypt and decrypt', () => {
      const { aes } = crypto
      const data = 'plain text'
      const encrypted = aes.encrypt(data)
      expect(encrypted).toBeDefined()
      expect(aes.decrypt(encrypted).toString('utf8')).toBe('plain text')
    })

    test('decrypt', () => {
      const { aes } = crypto

      const buf = Buffer.concat([
        Buffer.from('000000000000', 'utf8'),
        Buffer.from('cddab445' + '18afb73b7c46bf3185d58d434d5129fc', 'hex')
      ])

      const encrypted = {
        key: Buffer.from('00000000000000000000000000000000', 'utf8'),
        encrypted: base64url.encode(buf)
      }

      expect(aes.decrypt(encrypted).toString('utf8')).toBe('test')
    })

    test('encrypt', () => {
      const { aes } = crypto

      const key = Buffer.from('00000000000000000000000000000000', 'utf8')
      const iv = Buffer.from('000000000000', 'utf8')

      const encrypted = aes.encrypt('test', key, iv)
      const decoded = Buffer.from(encrypted.encrypted, 'base64')

      expect(decoded.toString('hex')).toBe(
        iv.toString('hex') + 'cddab445' + '18afb73b7c46bf3185d58d434d5129fc'
      )
    })

    test('android encrypted data', () => {
      const { aes } = crypto
      const key = Buffer.from(
        'Y77r5laMw8z_WICpfVxi-QVzv4rkT95f5FTKHgC3gl0',
        'base64'
      )

      const encrypted = '5-kfBtiMtQsG8Ue_7L-GgvgSA2RVvayzT00BKSXLGSb4vGGYVfk'

      expect(aes.decrypt({ encrypted, key }).toString('utf8')).toBe(
        'plain text'
      )
    })

    test('ios cryptokit encrypted data', () => {
      const { aes } = crypto
      const keyBase64 = 'Zg/FuuzGwPGQ20JEDKUnoHokRHAoBa3FP6mpStgh3bg='
      const nonceBase64 = 'ssMqhXLYAeNngTIz'
      const encBase64 = 'a4d0QSBeFQ=='
      const tagBase64 = 'BhIM8hecOyeggvtiDGJBxg=='
      const decr = 'message'

      const key = Buffer.from(keyBase64, 'base64')
      const encrypted = base64url.encode(
        Buffer.concat([
          Buffer.from(nonceBase64, 'base64'),
          Buffer.from(encBase64, 'base64'),
          Buffer.from(tagBase64, 'base64')
        ])
      )

      expect(aes.decrypt({ encrypted, key }).toString('utf8')).toBe(decr)
    })
  })

  test('encodeAndDecode__string__base64UrlSafeWithoutPadding', () => {
    const testString =
      'fdsfdsafdsaff|https://cdsf.fdsf.fdsfsd.com/rep|https://lfdjsfjsdfj.fdskfjdsk.com/license'
    const urlSafeString =
      'ZmRzZmRzYWZkc2FmZnxodHRwczovL2Nkc2YuZmRzZi5mZHNmc2QuY29tL3JlcHxodHRwczovL2xmZGpzZmpzZGZqLmZkc2tmamRzay5jb20vbGljZW5zZQ'

    const encode = base64url.encode(testString)
    expect(encode).toBe(urlSafeString)

    expect(base64url.decode(encode).toString('utf-8')).toBe(testString)
    expect(Buffer.from(urlSafeString, 'base64').toString('utf-8')).toBe(
      testString
    )
  })
})
