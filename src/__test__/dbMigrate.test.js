const path = require('path')
const dbMigrate = require('../dbMigrate')

describe('dbMigrate', () => {
  test('should work', async () => {
    jest.setTimeout(200000)
    try {
      await dbMigrate(
        path.resolve(__dirname, './migrations'),
        'postgres://dbmigrate_test:dbmigrate_test@localhost:5434/dbmigrate_test'
      )
    } catch (error) {
      // expect(res).toBe(false) TODO: fails for some reason
    }
  })
})
