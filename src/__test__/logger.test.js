describe('logger', () => {
  beforeEach(() => {
    global.console = td.object(console)
  })

  test('should log stack from Error', () => {
    const { logger } = require('../logger').loggerWithFormat('combine')
    logger.info(Error('error'))
    td.verify(global.console._stdout.write(td.matchers.contains('[STACK]')))
  })

  test('should contain level', () => {
    const { logger } = require('../logger').loggerWithFormat('combine')
    logger.error('message')
    td.verify(global.console._stdout.write(td.matchers.contains('[ERROR]')))
  })

  test('should log stack from Error', () => {
    const logger = require('../logger')
    logger.info(Error('error'))
    td.verify(global.console._stdout.write(td.matchers.contains('"stack":')))
  })

  test('should contain level', () => {
    const logger = require('../logger')
    logger.error('message')
    td.verify(
      global.console._stdout.write(td.matchers.contains('"level":"error"'))
    )
  })
})
