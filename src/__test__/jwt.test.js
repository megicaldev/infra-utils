const fs = require('fs')
const jsonwebtoken = require('jsonwebtoken')
const path = require('path')
const publicKey = fs.readFileSync(
  path.resolve(__dirname, './testKeys/public.pem')
)
const wrongPublicKey = fs.readFileSync(
  path.resolve(__dirname, './testKeys/wrongPublic.pem')
)
const privateKey = fs.readFileSync(
  path.resolve(__dirname, './testKeys/private.pem')
)

const jwt = require('../jwt')

describe('jwt', () => {
  describe('getCrendentials', () => {
    it('should return token', () => {
      const someToken = 'some_token'
      const req = {
        headers: {
          authorization: `Bearer ${someToken}`
        }
      }

      const token = jwt.getCredentials(req)
      expect(token).toEqual(someToken)
    })

    it('should fail when authorization header is missing', () => {
      expect(() => jwt.getCredentials({})).toThrowError(
        'AUTHORIZATION_REQUIRED'
      )
    })

    it('should fail when authorization schema does not match "Bearer" ', () => {
      const token = 'some_token'
      const req = { headers: { authorization: `Bear ${token}` } }

      expect(() => jwt.getCredentials(req)).toThrowError(
        'CREDENTIALS_BAD_SCHEME'
      )
    })
  })

  describe('getToken', () => {
    const invalidToken = 'some_token'
    const options = {
      algorithms: ['RS256'],
      issuer: 'haipro',
      audience: 'track',
      maxAge: '1m'
    }

    it('should fail if credentials is not given', () => {
      expect(() => jwt.getToken(null, options)).toThrowError(
        'CREDENTIALS_REQUIRED'
      )
    })

    it('should fail if token is invalid', () => {
      expect(() => jwt.getToken(invalidToken, options)).toThrowError(
        'INVALID_TOKEN'
      )
    })

    it('should fail if "kid" is missing from headers', () => {
      const credentials = jsonwebtoken.sign({}, privateKey, {
        algorithm: 'RS256'
      })

      expect(() => jwt.getToken(credentials, options)).toThrowError(
        'KID_NOT_FOUND'
      )
    })

    it('should fail if "kid" does not match public keys', () => {
      const credentials = jwt.createToken({
        privateKey,
        audience: 'track',
        issuer: 'some_kid'
      })

      expect(() => jwt.getToken(credentials, options)).toThrowError(
        'PUBLIC_KEY_NOT_FOUND'
      )
    })

    it('should fail if public key is invalid', () => {
      const credentials = jwt.createToken({
        privateKey,
        issuer: 'haipro',
        audience: 'track'
      })

      expect(() =>
        jwt.getToken(credentials, options, { haipro: 'wrongPublicKey' })
      ).toThrowError('AUTHORIZATION_REQUIRED')
    })

    it('should fail if private key does not match public key', () => {
      const credentials = jwt.createToken({
        privateKey,
        issuer: 'haipro',
        audience: 'track'
      })

      expect(() =>
        jwt.getToken(credentials, options, { haipro: wrongPublicKey })
      ).toThrowError('AUTHORIZATION_REQUIRED')
    })

    it('should fail if audience does not match', () => {
      const credentials = jwt.createToken({
        privateKey,
        audience: 'flow',
        issuer: 'haipro'
      })

      expect(() =>
        jwt.getToken(credentials, options, { haipro: publicKey })
      ).toThrowError('AUTHORIZATION_REQUIRED')
    })

    it('should fail if token is expired', () => {
      const credentials = jwt.createToken({
        privateKey,
        audience: 'track',
        issuer: 'haipro'
      })

      expect(() =>
        jwt.getToken(
          credentials,
          { ...options, maxAge: '0m' },
          { haipro: publicKey }
        )
      ).toThrowError('AUTHORIZATION_REQUIRED')
    })

    it('should return token', () => {
      const start = Math.floor(Date.now() / 1000) - 1
      const credentials = jsonwebtoken.sign({}, privateKey, {
        audience: 'track',
        issuer: 'haipro',
        algorithm: 'RS256',
        keyid: 'haipro'
      })

      const token = jwt.getToken(credentials, options, {
        haipro: publicKey
      })

      expect(token.aud).toEqual('track')
      expect(token.iss).toEqual('haipro')

      expect(token.iat).toBeGreaterThan(start)
    })
  })
})
