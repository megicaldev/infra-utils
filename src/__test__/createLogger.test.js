const createLogger = require('../createLogger')(true, 'json', 'info')(
  __filename
)
describe('createLogger', () => {
  beforeEach(() => {
    global.console = td.object(console)
  })
  test('create logger', () => {
    const logger = createLogger('tag')
    logger.error('create')

    td.verify(
      global.console._stdout.write(td.matchers.contains('"level":"error"'))
    )
    td.verify(global.console._stdout.write(td.matchers.contains('(tag)')))
    td.verify(global.console._stdout.write(td.matchers.contains('create')))
    td.verify(
      global.console._stdout.write(td.matchers.contains('createLogger'))
    )
  })
})
