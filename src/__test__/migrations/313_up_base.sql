CREATE TABLE test_table (
  id SERIAL PRIMARY KEY,
  uuid UUID UNIQUE NOT NULL,
  created_at timestamp with time zone NOT NULL
);