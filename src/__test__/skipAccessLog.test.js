const skipAccessLog = require('../skipAccessLog')
describe('skip access log', () => {
  test('should skip health', () => {
    const skip = skipAccessLog(true, ['health'])
    expect(skip({ url: 'health' }, {})).toBe(true)
  })

  test('should skip multiple urls', () => {
    const skip = skipAccessLog(true, ['first', 'second'])
    expect(skip({ url: 'first' }, {})).toBe(true)
    expect(skip({ url: 'second' }, {})).toBe(true)
    expect(skip({ url: 'thirt' }, {})).toBe(false)
  })

  test('should not skip health', () => {
    const skip = skipAccessLog(false, ['health'])
    expect(skip({ url: 'health' }, {})).toBe(false)
  })

  test('should not skip health', () => {
    const skip = skipAccessLog(true, ['health'])
    expect(skip({ url: 'some' }, {})).toBe(false)
  })

  test('should skip healthcheck', () => {
    const skip = skipAccessLog(true, ['health'])
    expect(skip({ url: 'healthcheck' }, {})).toBe(true)
  })
})
