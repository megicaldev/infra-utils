const util = require('util')

const migrations = require('sql-migrations')
const parse = require('pg-connection-string').parse
const Adapter = require('sql-migrations/adapters/pg')

const logger = require('./logger')

const formatArgs = args => {
  return [util.format.apply(util.format, Array.prototype.slice.call(args))]
}

const dbLogger = {
  log: (...args) => {
    logger.debug.apply(logger, formatArgs(['[DB_MIGRATE]', ...args]))
  },
  error: (...args) => {
    logger.error.apply(logger, formatArgs(['[DB_MIGRATE]', ...args]))
  }
}

module.exports = async (migrationsDir, databaseUrl) => {
  if (!migrationsDir || !databaseUrl) {
    throw Error(
      `Missing migrations parameters -> migrationsDir: ${migrationsDir}, databaseUrl: ${databaseUrl}`
    )
  }
  const dbConfig = parse(databaseUrl)

  const config = Object.assign({}, dbConfig, {
    migrationsDir,
    adapter: 'pg',
    db: dbConfig.database
  })

  const dbMigrate = async () => {
    migrations.setLogger(dbLogger)
    const adapter = Adapter(config, dbLogger)
    logger.info('[DB_MIGRATE] Run database migrations')

    await migrations.migrate(config, adapter)
    logger.info('[DB_MIGRATE] Completed database migrations')
    return true
  }

  return dbMigrate()
}
