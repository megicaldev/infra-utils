const skipUrls = (req, urls) =>
  urls.some((url) => req.url.toLowerCase().includes(url))

const skipAccessLog = (DISABLE_ACCESS_LOG_FOR_HEALTH, urls) => (req, res) => {
  if (!DISABLE_ACCESS_LOG_FOR_HEALTH || !req || typeof req.url !== 'string') {
    return false
  } else if (!skipUrls(req, urls)) {
    return false
  } else {
    return true
  }
}

module.exports = skipAccessLog
