const path = require('path')
const util = require('util')
const loggerWithFormat = require('./logger').loggerWithFormat

module.exports = (DEVMODE, LOGGER_FORMAT, LOGGER_LEVEL) => {
  const { logger, logStream } = loggerWithFormat(LOGGER_FORMAT, LOGGER_LEVEL)

  return (filename) => {
    if (!filename) {
      throw Error('filename missing')
    }
    const scriptName = path.basename(filename)
    const loggerInstances = {}
    return (...args) => {
      let tag = ''
      args.map((arg) => {
        if (typeof arg === 'function') {
          tag += ` [${arg.name}]`
        } else if (typeof arg === 'string') {
          tag += ` (${arg})`
        } else {
          throw Error('Tag format not supported')
        }
      })
      const tagString = `[${scriptName}]${tag}`
      if (loggerInstances[tagString]) {
        return loggerInstances[tagString]
      } else {
        loggerInstances[tagString] = Object.keys(logger.levels).reduce(
          (acc, level) => {
            acc[level] = (msg, ...rest) => {
              if (DEVMODE) {
                // Crash only in DEVMODE because too lazy to search code for usage
                // Logger has never logged extra parameters
                if (rest.length > 0) {
                  throw Error('Logger supports only on parameter')
                }
              }

              if (typeof msg !== 'string') {
                msg = util.inspect(msg)
              }
              return logger[level](`${tagString} ${msg}`)
            }
            return acc
          },
          { level: logger.level, logStream }
        )
        return loggerInstances[tagString]
      }
    }
  }
}
